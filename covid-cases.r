#!/usr/bin/env Rscript

library(tidyverse);
library(R0);
library(viridis);

## RStudio -- set the working directory to the location of *this* file
setwd(dirname(rstudioapi::getActiveDocumentContext()$path));

## This is designed to work with the MOH COVID-19 cases CSV file
## [https://www.health.govt.nz/our-work/diseases-and-conditions/
##  covid-19-novel-coronavirus/covid-19-data-and-statistics/covid-19-case-demographics]

list.files("data", pattern="covid_cases.*\\.csv") %>% 
  sub("^covid_cases_","", .) %>% sub("\\.csv", "", .) %>%
  lubridate::as_date() %>% max() %>% as.character() -> maxDate;

cases.all.tbl <- read_csv(sprintf("data/covid_cases_%s.csv", maxDate)) %>%
  filter(DHB != "Managed Isolation & Quarantine", is.na(Historical),
         `Report Date` > "2021-07-01",
         `Report Date` <= (max(`Report Date`)))

firstDate <- min(cases.all.tbl$`Report Date`);
maxDateDiff <- lubridate::as_date(maxDate) - lubridate::as_date(firstDate);

cases.all.tbl %>%
  mutate(day = as.integer(`Report Date` - min(`Report Date`))) %>%
  group_by(day) %>%
  dplyr::summarise(newCases = n()) -> cases.df

# fit first degree polynomial equation:
fit  <- glm(newCases ~ day, data=filter(cases.df, (day >= 2) & (day <= 9)));
# exponential [doesn't look right on the graph]
fit2 <- glm(newCases ~ exp(day), data=cases.df);
# exponential (matching a linear function in log space)
cases.df2 <- cases.df[-1,];

IPs <- c(2, 9, 23, 44, maxDateDiff - 1); ## Inflection points
significant.dates <- c("NZ L4" = "2021-08-17",
                       "NZ L3" = "2021-09-01",
                       #"NZ L2" = "2021-09-07",
                       "Auck L3" = "2021-09-21",
                       "Super Saturday" = "2021-10-16");
significant.labels <- names(significant.dates);
significant.dates <- significant.dates %>% lubridate::as_date() + 9 - firstDate;
names(significant.dates) <- significant.labels;
curveCols <- viridis(length(IPs), option = "H");
fits <- lapply(2:length(IPs), function(x){
  glm(log(newCases) ~ day, data=filter(cases.df, (day >= IPs[x-1]) & (day <= IPs[x])));
});
## generation time assumes an average transfer time of 3 days, with tail of 14 days
## From Sep 03 questions about infectious period, infectious period can be longer [buffer to 14 days]
ests <- lapply(2:length(IPs), function(x){
  est.R0.EG(cases.df$newCases, generation.time("gamma", c(3, 1.6)),
            begin=min(which(cases.df$day >= IPs[x-1])),
            end=min(which(cases.df$day >= IPs[x])));
});

xvals <- seq(-5,max(cases.df$day) + 15,0.25);
yRange <- c(0, max(130, max(cases.df$newCases)*1.2));

png("fit.png", width=2400, height=1200, pointsize = 32);
par(mar=c(4.5,4.5,3.5,0.5), family="Pointilised", cex.main=1.5);
plot(newCases ~ day, data = cases.df, pch=19, xlim=range(xvals),
     ylim=yRange, main = sprintf("Daily Community Cases Since Outbreak Start (as at 9am %s)", maxDate),
     axes=FALSE);
axis(2);
axis(1, at=axTicks(1), labels = format(axTicks(1) + min(cases.all.tbl$`Report Date`), "%d %b"));
## plot interval projections
for(ix in (2:length(IPs) - 1)){
  ## back projection
  backRange <- which(xvals <= IPs[ix]);
  if(ix > 1){
    backRange <- intersect(backRange, which(xvals >= IPs[ix-1]));
  }
  lines(xvals[backRange],
        exp(predict(fits[[ix]], newdata=data.frame(day=xvals[backRange]))),
        col=curveCols[ix], lwd=2, lty = "dashed")
  ## interval projection
  lines(xvals[(xvals >= IPs[ix]) & (xvals <= IPs[ix+1])],
        exp(predict(fits[[ix]], newdata=data.frame(day=xvals[(xvals >= IPs[ix]) & (xvals <= IPs[ix+1])]))),
        col=curveCols[ix], lwd=6)
  ## forward projection
  fwdRange <- which(xvals >= IPs[ix+1]);
  if(ix < (length(IPs)-1)){
    fwdRange <- intersect(fwdRange, which(xvals <= IPs[ix+2]));
  }
  lines(xvals[fwdRange],
        exp(predict(fits[[ix]], newdata=data.frame(day=xvals[fwdRange]))),
        col=curveCols[ix], lwd=2, lty="dashed")
}
## replot points (over curves)
points(newCases ~ day, data = cases.df, pch=19);
points(newCases ~ day, data = cases.df, pch=19, col=viridis(7), cex=0.5);
## Plot first R value
text(mean(IPs[1:2]), max(yRange)-50, adj = c(1,1),
     labels=sprintf("p = %0.2g\na = %0.3g\nRe = %0.2g\n[%0.3g, %0.3g]",
                    summary(fits[[1]])$coefficients[2,4],
                    exp(summary(fits[[1]])$coefficients[2,1]),
                    ests[[1]]$R,
                    ests[[1]]$conf.int[1],
                    ests[[1]]$conf.int[2]),
     col=curveCols[1]);
## plot remaining R values
for(xi in 3:length(IPs) - 1){
text(mean(IPs[xi:(xi+1)]), max(yRange) -50, adj = c(0,1),
     labels=sprintf("p = %0.2g\na = %0.3g\nRe = %0.2g\n[%0.3g, %0.3g]",
                    summary(fits[[xi]])$coefficients[2,4],
                    exp(summary(fits[[xi]])$coefficients[2,1]),
                    ests[[xi]]$R,
                    ests[[xi]]$conf.int[1],
                    ests[[xi]]$conf.int[2]),
  col=curveCols[xi]);
}
## Plot counts for last few days
cases.last <- tail(cases.df, 5);
text(x=cases.last$day, y=cases.last$newCases, labels=cases.last$newCases,
     pos=1, cex=0.71);
abline(v=significant.dates, lty = "dashed", lwd = 5, col="#00000040");
rect(xleft = significant.dates-9, xright=significant.dates, 
     ybottom = 0, ytop = max(yRange), col="#00000020", border=NA);
text(x=significant.dates, y=0, 
     labels = names(significant.dates), adj = c(0.5,0.5));
legend("top", legend = sprintf("Day %d-%d", head(IPs, -1), tail(IPs, -1)), bg="#FFFFFFA0",
       fill=curveCols, inset=0.05, title = "Exponential Curve Fits", horiz=TRUE);
invisible(dev.off());

png("fitLog.png", width=2400, height=1200, pointsize = 32);
plot(log(newCases) ~ day, data=cases.df, xaxt="n", yaxt="n", ylim=c(0,5), ylab="New Cases",
     xlab="Day");
axis(1, at=axTicks(1), labels = format(axTicks(1) + min(cases.all.tbl$`Report Date`), "%d %b"));
axis(2, at=log(c(1,2,5,10,20,50,100,200)), lwd=3, labels = c(1,2,5,10,20,50,100,200));
axis(2, at=log(rep(1:9, 4) * 10^rep(0:3, each=9)), labels=FALSE);
lines(xvals,
      (predict(fit3, newdata=data.frame(day=xvals))), col="orangered", lwd=3);
lines(xvals,
      (predict(fit4, newdata=data.frame(day=xvals))), col="darkolivegreen", lwd=3);
lines(xvals,
      (predict(fit5, newdata=data.frame(day=xvals))), col="blue", lwd=3);
abline(v=c(2,9,23), lty="dashed", col="#00000080");
invisible(dev.off());


exp(predict(fit4, newdata=data.frame(day=39)))

summary(fit3)$coefficients[2,4] / summary(fit)$coefficients[2,4]

summary(fit);
summary(fit3);
