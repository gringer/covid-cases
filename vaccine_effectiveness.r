## RStudio -- set the working directory to the location of *this* file
setwd(dirname(rstudioapi::getActiveDocumentContext()$path));

## based on tenth-rounded proportions from 
## https://www.cidrap.umn.edu/news-perspective/2021/07/study-2-covid-vaccine-doses-much-more-effective-1-against-delta
effectiveness.d1 <- 0.50;
effectiveness.d2 <- 0.90;

## based on MoH COVID-19 'demographics' figures for 2021-Nov-03

cases.all <- 3733;
cases.d1 <- 293 + 417;
cases.d2 <- 100 + 274;
cases.d0 <- cases.all - (cases.d1 + cases.d2);

hosp.all <- 272;
hosp.d1 <- 26 + 28;
hosp.d2 <- 5 + 6;
hosp.d0 <- hosp.all - (hosp.d1 + hosp.d2);

## based on OWID figures for 2021-Oct-15
vaccinated.d2 <- 547;
vaccinated.d1 <- 179;
vaccinated.d0 <- 1000 - (vaccinated.d1 + vaccinated.d2);
vaccinated.all <- 1000;

## Assume cases.d0 == exposed.d0
## And that vaccinated proportions equal exposed proportions
exposed.d0 <- cases.d0
exposed.d1 <- round((cases.d0 / vaccinated.d0) * vaccinated.d1);
exposed.d2 <- round((cases.d0 / vaccinated.d0) * vaccinated.d2);
exposed.all <- exposed.d0 + exposed.d1 + exposed.d2;

# ## Alternate construction, assuming reported vaccine effectiveness is correct
# exposed.d2 <- cases.d2 / (1 - effectiveness.d2);
# exposed.d1 <- cases.d1 / (1 - effectiveness.d1);
# exposed.d0 <- cases.d0;
# exposed.all <- exposed.d2 + exposed.d1 + exposed.d0;

matrix(c(hosp.d0, hosp.d1, hosp.d2,
         cases.d0, cases.d1, cases.d2,
         exposed.d0, exposed.d1, exposed.d2,
         vaccinated.d0, vaccinated.d1, vaccinated.d2), ncol=4,
       dimnames = list(c("0 doses", "1 dose", "2 doses"),
                       c("hospitalised", "cases",
                         "exposed\n[estimated]", "vaccinated"))) -> case.mat;

png("case_vaccine_demographics.png", width=2400, height=1200, pointsize=24);
layout(matrix(1:2,nrow=2), heights = c(0.3,0.7));
par(mar=c(3,4,2,2), family="Pointilised");
res <- barplot(case.mat[,4,drop=FALSE], horiz=TRUE,
        beside=TRUE, main="Vaccinated people per 1000 [as at 2021-Oct-15]");
text(y=res, x=case.mat[,4], labels=case.mat[,4], pos=2,
     col = c("grey80", "grey20", "grey20"), cex=1)
par(mar=c(5,4,1.5,2));
res <- barplot(case.mat[,-4], beside=TRUE, legend=TRUE, horiz=TRUE,
        xlab = "Number of people", main = "Aotearoa Case Vaccine Demographics [2021-Nov-03]",
        args.legend = list(x="bottomright", inset=0.05));
text(y=res, x=case.mat[,-4], cex=1,
     labels=ifelse(1:9 > 6, case.mat[,-4], 
                   sprintf("%s (%0.1f %%)", case.mat[,-4],
                            100 * case.mat[,-4] / case.mat[,-1])),
     pos = ifelse(case.mat[,-4] < max(case.mat[,-4]/2), 4, 2),
     col = ifelse(case.mat[,-4] < max(case.mat[,-4]/2), 
                  "grey20", c("grey80", "grey20", "grey20")));
invisible(dev.off());